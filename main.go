package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

type User struct {
	ID string `uri:"id" binding:"required,uuid"`
	Name string `uri:"name"`
}

func main() {

	appName := "simple-http-server"
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/etc/" + appName)
	viper.AddConfigPath("$HOME/." + appName)
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Unable to load config file %s \n", err))
	}

	fmt.Println(viper.Get("app"))


	r := gin.Default()
	//Default health endpoint returning 200 and a json message
	r.GET("/health", func(c *gin.Context) {
		c.JSON(200, gin.H{"message": "ok",})
	})


	//Create a /v1 group of endpoints
	v1 := r.Group("/v1")
	{
		//Health endpoint running on group /v1
		v1.GET("/health", func(c *gin.Context) {
			c.JSON(200, gin.H{"version": "v1", "message": "ok"})
		})
	}

	//Create a /v2 group of endpoints
	v2  := r.Group("/v2")
	{
		//Health endpoint running on group /v2
		v2.GET("/health", func(c *gin.Context) {
			c.JSON(200, gin.H{"version": "v2", "message": "ok"})
		})

		//Create an endpoint that binds several URI paramters into a User struct
		v2.GET("/api/:id/:name/list", func(c *gin.Context) {
			var user User
			if err := c.ShouldBindUri(&user); err != nil {
				c.JSON(200, gin.H{"msg": err})
				return
			}
			c.JSON(200, gin.H{"get for user": user.Name, "uuid": user.ID})
		})
	}

	r.Run()

}
